# README #

### What is this repository for? ###

* This is the server-side code of the Tomman project


### How do I get set up? ###

* Run org.gbe.capstone.tomman.webservice.Application.java
* In order to populate the database with two doctors and 3 patients, run the Junit test :
org.gbe.capstone.tomman.test.TommanSvcTest.testAdminFunctionalities()