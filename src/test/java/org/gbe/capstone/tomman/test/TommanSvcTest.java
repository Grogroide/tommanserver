package org.gbe.capstone.tomman.test;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;

import org.junit.Test;

import org.gbe.capstone.tomman.webservice.repos.EatingDifficulty;
import org.gbe.capstone.tomman.webservice.repos.Medication;
import org.gbe.capstone.tomman.webservice.repos.PainLevel;
import org.gbe.capstone.tomman.webservice.retrofit.*;

import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.ApacheClient;
import retrofit.http.Path;

import com.google.gson.JsonObject;

/**
 * 
 * This integration test sends a POST request to the VideoServlet to add a new
 * video and then sends a second GET request to check that the video showed up
 * in the list of videos. Actual network communication using HTTP is performed
 * with this test.
 * 
 * The test requires that the VideoSvc be running first (see the directions in
 * the README.md file for how to launch the Application).
 * 
 * To run this test, right-click on it in Eclipse and select
 * "Run As"->"JUnit Test"
 * 
 * Pay attention to how this test that actually uses HTTP and the test that just
 * directly makes method calls on a VideoSvc object are essentially identical.
 * All that changes is the setup of the videoService variable. Yes, this could
 * be refactored to eliminate code duplication...but the goal was to show how
 * much Retrofit simplifies interaction with our service!
 * 
 * @author jules
 *
 */
public class TommanSvcTest {

	private final String ADMIN_USERNAME = "admin";
	private final String ADMIN_PASSWORD = "admin";
	
	private final String DOCTOR_USERNAME = "doctor1";
	private final String DOCTOR2_USERNAME="doctor2";
	private final String DOCTOR_PASSWORD = "doctor";
	
	private final String PATIENT_USERNAME = "patient1";
	private final String PATIENT_PASSWORD = "patient";
	
	private final String CLIENT_ID = "mobile";

	private final String TEST_URL = "https://localhost:8443";
	
	
	private TommanSvcApi tommanAsAdmin = new SecuredRestBuilder()
		.setLoginEndpoint(TEST_URL + TommanSvcApi.TOKEN_PATH)
		.setUsername(ADMIN_USERNAME)
		.setPassword(ADMIN_PASSWORD)
		.setClientId(CLIENT_ID)
		.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
		.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build().create(TommanSvcApi.class);
	
	private TommanSvcApi tommanAsDoctor = new SecuredRestBuilder()
		.setLoginEndpoint(TEST_URL + TommanSvcApi.TOKEN_PATH)
		.setUsername(DOCTOR_USERNAME)
		.setPassword(DOCTOR_PASSWORD)
		.setClientId(CLIENT_ID)
		.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
		.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build().create(TommanSvcApi.class);
	
	private TommanSvcApi tommanAsDoctor2 = new SecuredRestBuilder()
		.setLoginEndpoint(TEST_URL + TommanSvcApi.TOKEN_PATH)
		.setUsername(DOCTOR2_USERNAME)
		.setPassword(DOCTOR_PASSWORD)
		.setClientId(CLIENT_ID)
		.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
		.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build().create(TommanSvcApi.class);
	
	private TommanSvcApi tommanAsPatient = new SecuredRestBuilder()
		.setLoginEndpoint(TEST_URL + TommanSvcApi.TOKEN_PATH)
		.setUsername(PATIENT_USERNAME)
		.setPassword(PATIENT_PASSWORD)
		.setClientId(CLIENT_ID)
		.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
		.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build().create(TommanSvcApi.class);
	
	private MedicationDTO test_medication = new MedicationDTO("Guronsan", "One per hangover");
	

	
	@Test
	public void testResetRepos() throws Exception {
		tommanAsAdmin.flush();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		assertTrue(dList.size() == 0);
		assertTrue(pList.size() == 0);
		tommanAsAdmin.initTest();
		dList = tommanAsAdmin.getAllDoctors();
		pList = tommanAsAdmin.getAllPatients();
		assertTrue(dList.size() == 2);
		assertTrue(pList.size() == 3);
	}
	@Test
	public void testAdminFunctionalities() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		assertTrue(dList.size() == 2);
		assertTrue(pList.size() == 3);
		DoctorDTO d1 = findByUsername(dList, DOCTOR_USERNAME);
		assertFalse(d1 == null);
		for (Person patient : pList) {
			tommanAsAdmin.assignDoctorToPatient(d1.getId(), patient.getId());
		}
		assertTrue(tommanAsDoctor.getPatientList().size() == 3);
		assertTrue(tommanAsPatient.getPatientInfo().getDoctors().contains(d1.getId()));
	}
	
	@Test
	public void testLoginAsDoctor() throws Exception {
		TommanApiRole role = tommanAsDoctor.login();
		assertEquals(role, TommanApiRole.Doctor);
	}
	
	@Test 
	public void testLoginAsPatient() throws Exception {
		assertTrue(tommanAsPatient.login() == TommanApiRole.Patient);
	}
	
	@Test
	public void testSearchPatientByName() {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		assertTrue(dList.size() == 2);
		assertTrue(pList.size() == 3);
		DoctorDTO d1 = findByUsername(dList, DOCTOR_USERNAME);
		assertFalse(d1 == null);
		for (Person patient : pList) {
			tommanAsAdmin.assignDoctorToPatient(d1.getId(), patient.getId());
		}
		assertTrue(tommanAsDoctor.getPatientList().size() == 3);
		assertTrue(tommanAsPatient.getPatientInfo().getDoctors().contains(d1.getId()));
		pList.clear();
		pList = tommanAsDoctor.searchPatientByName("Furio");
		assertTrue(pList.size() == 1);
		pList.clear();
		pList = tommanAsDoctor.searchPatientByName("Paulie");
		assertTrue(pList.size() == 1);
		pList.clear();
		pList = tommanAsDoctor.searchPatientByName("bibi");
		assertTrue(pList.size() == 0);
	}
	
	@Test
	public void testGetDoctor() throws Exception {
		DoctorDTO doc = tommanAsDoctor.getDoctor();
		assertEquals(doc.getUserName(), DOCTOR_USERNAME);
	}
	
	@Test
	public void testGetAllMedicationAsDoctor() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<MedicationDTO> pharmacy = tommanAsDoctor.getMedications();
		assertTrue(!pharmacy.isEmpty());
	}
	
	@Test
	public void testGetAllMedicationAsPatient() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<MedicationDTO> pharmacy = new HashSet<MedicationDTO>();
		try
		{
			pharmacy = tommanAsPatient.getMedications();
			fail("The patient should not have access to the whole pharmacy\r\n" + "Pharmacy size : " + pharmacy.size());
		}
		catch(RetrofitError e)
		{
			JsonObject body = (JsonObject)e.getBodyAs(JsonObject.class);
			assertEquals("access_denied", body.get("error").getAsString());
		}
	}
	
	@Test
	public void testAddMedicationAsDoctor() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		MedicationDTO med = new MedicationDTO("Coffee", "Two cups at 8AM, two more every hour");
		MedicationDTO rv = tommanAsDoctor.addMedication(med);
		assertTrue(rv.getId() != 0);
		rv = tommanAsDoctor.addMedication(test_medication);
		assertTrue(rv.getId() != 0);
		long test_med_id = rv.getId();
		rv = tommanAsDoctor.addMedication(test_medication);
		assertEquals(test_med_id, (long)rv.getId());
	}
	
	@Test
	public void testRetrieveOneMedicationAsDoctor() throws Exception{
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<MedicationDTO> pharmacy = tommanAsDoctor.getMedications();
		for (MedicationDTO medication : pharmacy) {
			long _id = medication.getId();
			MedicationDTO temp = tommanAsDoctor.getMedication(_id);
			assertTrue(temp != null);
		}
	}
	
	@Test
	public void testRetrieveMyDataAsPatient() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Person p = tommanAsPatient.getPatientInfo();
		assertEquals(p.getUserName(), PATIENT_USERNAME);
	}
	
	@Test
	public void testRetrieveMyPatientsAsDoctor() throws Exception { 
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		DoctorDTO d1 = findByUsername(dList, DOCTOR_USERNAME);
		for (Person patient : pList) {
			tommanAsAdmin.assignDoctorToPatient(d1.getId(), patient.getId());
		}
		Collection<PatientDTO> list = tommanAsDoctor.getPatientList();
		for (PatientDTO patient : list) {
			PatientDTO p = tommanAsDoctor.getPatientInfo(patient.getId());
			assertEquals(p.getId(), patient.getId());
		}
	}
	
	@Test
	public void testPrescribe() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		if (tommanAsDoctor.getPatientList().isEmpty())		// should be always true now
		{
			tommanAsAdmin.assignDoctorToPatient(
					findByUsername(dList, DOCTOR_USERNAME).getId(),
					findByUsername(pList, PATIENT_USERNAME).getId()
					);
		}
		tommanAsDoctor.addMedication(new MedicationDTO("Coffee", "Three cups in the morning"));
		PatientDTO pat = findByUsername(tommanAsDoctor.getPatientList(), PATIENT_USERNAME);
		for(MedicationDTO med : tommanAsDoctor.getMedications()){
			assertTrue(tommanAsDoctor.prescribe(pat.getId(), med.getId()));
		}
		Collection<MedicationDTO> prescriptions = tommanAsPatient.getPatientInfo().getPrescriptions();
		assertTrue(prescriptions.size() > 0);
	}
	
	@Test
	public void testUnprescribe() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		if (tommanAsDoctor.getPatientList().isEmpty())		// should be always true now
		{
			tommanAsAdmin.assignDoctorToPatient(
					findByUsername(dList, DOCTOR_USERNAME).getId(),
					findByUsername(pList, PATIENT_USERNAME).getId()
					);
		}
		tommanAsDoctor.addMedication(new MedicationDTO("Coffee", "Three cups in the morning"));
		PatientDTO pat = findByUsername(tommanAsDoctor.getPatientList(), PATIENT_USERNAME);
		for(MedicationDTO med : tommanAsDoctor.getMedications()){
			assertTrue(tommanAsDoctor.prescribe(pat.getId(), med.getId()));
		}
		Collection<MedicationDTO> prescriptions = tommanAsPatient.getPatientInfo().getPrescriptions();
		assertTrue(prescriptions.size() > 0);		
		
		for(MedicationDTO med : tommanAsDoctor.getMedications()){
			assertTrue(tommanAsDoctor.unprescribe(pat.getId(), med.getId()));
		}
		prescriptions = tommanAsPatient.getPatientInfo().getPrescriptions();
		assertTrue(prescriptions.size() == 0);
		
	}
	
	@Test
	public void testCheckIn() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Calendar cal = Calendar.getInstance();
		CheckInDTO c = new CheckInDTO(cal.getTime().getTime(), PainLevel.MODERATE, EatingDifficulty.NO);
		c = tommanAsPatient.checkIn(c);
		assertTrue(c.getId() != 0);
		Thread.sleep(1);
		c = new CheckInDTO(cal.getTime().getTime(), PainLevel.SEVERE, EatingDifficulty.SOME);
		c = tommanAsPatient.checkIn(c);
		assertTrue(c.getId() != 0);
		Collection<CheckInDTO> ciList = tommanAsPatient.getHistory();
		assertTrue(ciList.size() >= 2);
	}
	
	@Test
	public void testGetHistoryOfPatient() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Calendar cal = Calendar.getInstance();
		CheckInDTO c = new CheckInDTO(cal.getTime().getTime(), PainLevel.MODERATE, EatingDifficulty.NO);
		c = tommanAsPatient.checkIn(c);
		assertTrue(c.getId() != 0);
		DoctorDTO d1 = findByUsername(tommanAsAdmin.getAllDoctors(), DOCTOR_USERNAME);
		PatientDTO p1 = findByUsername(tommanAsAdmin.getAllPatients(),  PATIENT_USERNAME);
		tommanAsAdmin.assignDoctorToPatient(
				d1.getId(), 
				p1.getId() );
		Collection <CheckInDTO> ciList = tommanAsDoctor.getHistoryOfPatient(p1.getId());
		assertTrue(ciList.size() == 1);
	}
	
	@Test
	public void testAlertTrigger() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -12);
		CheckInDTO c = new CheckInDTO(cal.getTime().getTime(), PainLevel.SEVERE, EatingDifficulty.NO);
		tommanAsPatient.checkIn(c);
		cal.add(Calendar.HOUR_OF_DAY, 4);
		c = new CheckInDTO(cal.getTime().getTime(), PainLevel.SEVERE, EatingDifficulty.NO);
		tommanAsPatient.checkIn(c);
		cal.add(Calendar.HOUR_OF_DAY, 4);
		c = new CheckInDTO(cal.getTime().getTime(), PainLevel.SEVERE, EatingDifficulty.NO);
		tommanAsPatient.checkIn(c);
		cal.add(Calendar.HOUR_OF_DAY, 4);
		c = new CheckInDTO(cal.getTime().getTime(), PainLevel.SEVERE, EatingDifficulty.NO);
		tommanAsPatient.checkIn(c);
		
		DoctorDTO d1 = findByUsername(tommanAsAdmin.getAllDoctors(), DOCTOR_USERNAME);
		PatientDTO p1 = findByUsername(tommanAsAdmin.getAllPatients(),  PATIENT_USERNAME);
		tommanAsAdmin.assignDoctorToPatient(
				d1.getId(), 
				p1.getId() );
		
		Collection <CheckInDTO> ciList = tommanAsDoctor.getHistoryOfPatient(p1.getId());
		assertTrue(ciList.size() == 4);
		cal.add(Calendar.DATE, -2);
		Collection <PatientDTO> alerts = tommanAsDoctor.getAlerts(cal.getTimeInMillis());
		assertTrue(alerts.size() > 0 );
		
	}
	
	@Test
	public void testCheckInWithMedications() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Calendar cal = Calendar.getInstance();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		tommanAsAdmin.assignDoctorToPatient(
				findByUsername(dList, DOCTOR_USERNAME).getId(),
				findByUsername(pList, PATIENT_USERNAME).getId()
				);
		PatientDTO pat = findByUsername(tommanAsDoctor.getPatientList(), PATIENT_USERNAME);
		MedicationDTO m = tommanAsDoctor.getMedications().iterator().next();
		assertEquals(PATIENT_USERNAME, pat.getUserName());
		assertTrue(tommanAsDoctor.prescribe(pat.getId(), m.getId()));
		
		/* Start patient activity*/
		PatientDTO pat_self = tommanAsPatient.getPatientInfo();
		MedicationDTO med_self = pat_self.getPrescriptions().iterator().next();
		
		CheckInDTO c = new CheckInDTO(cal.getTime().getTime(), PainLevel.MODERATE, EatingDifficulty.NO);
		TookMedicationDTO tm = new TookMedicationDTO(cal.getTimeInMillis() - 3600 * 1000, med_self.getId(), pat_self.getId(), 1);
		//Collection<TookMedicationDTO> tmList = new HashSet<TookMedicationDTO> ();
		c.getTookMedications().add(tm);
		//c.setTookMedications(tmList);
		c = tommanAsPatient.checkIn(c);
		assertTrue(c.getId() != 0);
		assertTrue(c.getTookMedications().size() == 0);
	}
	
	@Test
	public void testGetMedicationHistory() throws Exception {
		tommanAsAdmin.flush();
		tommanAsAdmin.initTest();
		Calendar cal = Calendar.getInstance();
		Collection<DoctorDTO> dList = tommanAsAdmin.getAllDoctors();
		Collection<PatientDTO> pList = tommanAsAdmin.getAllPatients();
		tommanAsAdmin.assignDoctorToPatient(
				findByUsername(dList, DOCTOR_USERNAME).getId(),
				findByUsername(pList, PATIENT_USERNAME).getId()
				);
		PatientDTO pat = findByUsername(tommanAsDoctor.getPatientList(), PATIENT_USERNAME);
		MedicationDTO m = tommanAsDoctor.getMedications().iterator().next();
		assertEquals(PATIENT_USERNAME, pat.getUserName());
		assertTrue(tommanAsDoctor.prescribe(pat.getId(), m.getId()));
		
		/* Start patient activity*/
		PatientDTO pat_self = tommanAsPatient.getPatientInfo();
		MedicationDTO med_self = pat_self.getPrescriptions().iterator().next();
		
		CheckInDTO c = new CheckInDTO(cal.getTime().getTime(), PainLevel.MODERATE, EatingDifficulty.NO);
		TookMedicationDTO tm = new TookMedicationDTO(cal.getTimeInMillis() - 3600 * 1000, med_self.getId(), pat_self.getId(), 1);
		//Collection<TookMedicationDTO> tmList = new HashSet<TookMedicationDTO> ();
		c.getTookMedications().add(tm);
		//c.setTookMedications(tmList);
		c = tommanAsPatient.checkIn(c);
		assertTrue(c.getId() != 0);
		assertTrue(c.getTookMedications().size() == 0);
		Collection<TookMedicationDTO> tmListAsPatient = tommanAsPatient.getMedicationHistory();
		assertTrue(tmListAsPatient.iterator().next().getMedication().getId() == m.getId());  //getTimestamp() == tm.getTimestamp());
		Collection<TookMedicationDTO> tmListAsDoctor = tommanAsDoctor.getMedicationHistoryOfPatient(pat.getId());
		assertTrue(tmListAsDoctor.iterator().next().getTimestamp() == tm.getTimestamp());
	}
	
	private <T extends Person> T findByUsername(Collection<T> c, String name){
		for (T p : c) {
			if (p.getUserName().equals(name))
				return p;
		}
		return null;
	}

}
