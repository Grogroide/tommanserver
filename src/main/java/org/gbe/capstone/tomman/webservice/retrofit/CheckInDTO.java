package org.gbe.capstone.tomman.webservice.retrofit;

import java.util.Collection;
import java.util.HashSet;

import org.gbe.capstone.tomman.webservice.repos.CheckIn;
import org.gbe.capstone.tomman.webservice.repos.EatingDifficulty;
import org.gbe.capstone.tomman.webservice.repos.PainLevel;

public class CheckInDTO {
	private long id;
	private long timestamp;
	private PainLevel painLevel;
	private EatingDifficulty eatingDifficulty;
	
	private Collection<TookMedicationDTO> tookMedications;
	
	public CheckInDTO() {
		this.tookMedications = new HashSet<TookMedicationDTO>();
	}

	public CheckInDTO(long ts, PainLevel p, EatingDifficulty e)
	{
		this.timestamp = ts;
		this.painLevel = p;
		this.eatingDifficulty = e;
		this.tookMedications = new HashSet<TookMedicationDTO>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public PainLevel getPainLevel() {
		return painLevel;
	}
	public void setPainLevel(PainLevel painLevel) {
		this.painLevel = painLevel;
	}
	public EatingDifficulty getEatingDifficulty() {
		return eatingDifficulty;
	}
	public void setEatingDifficulty(EatingDifficulty eatingDifficulty) {
		this.eatingDifficulty = eatingDifficulty;
	}
	public Collection<TookMedicationDTO> getTookMedications() {
		return tookMedications;
	}
	public void setTookMedications(Collection<TookMedicationDTO> tookMedications) {
		this.tookMedications = tookMedications;
	}
	
	
}
