package org.gbe.capstone.tomman.webservice.retrofit;


import com.google.common.base.Objects;

public class TookMedicationDTO {

	private long id;
	private long timestamp;
	private long medicationId;
	private long patientId;
	private int quantity;
	private MedicationDTO medication;
	
	public TookMedicationDTO(){}
	public TookMedicationDTO(long timestamp, long medicationId, long patientId, int quantity){
		this.timestamp = timestamp;
		this.medicationId = medicationId;
		this.patientId = patientId;
		this.quantity = quantity;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getMedicationId() {
		return medicationId;
	}
	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(timestamp, medicationId, patientId, quantity);
	}

	/**
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TookMedicationDTO) {
			TookMedicationDTO other = (TookMedicationDTO) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(timestamp, other.timestamp)
					&& Objects.equal(medicationId, other.medicationId)
					&& Objects.equal(patientId, other.patientId)
					&& Objects.equal(quantity, other.quantity);
		} else {
			return false;
		}
	}
	public MedicationDTO getMedication() {
		return medication;
	}
	public void setMedication(MedicationDTO medication) {
		this.medication = medication;
	}
	
}
