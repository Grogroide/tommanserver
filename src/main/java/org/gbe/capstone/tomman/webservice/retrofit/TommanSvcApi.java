package org.gbe.capstone.tomman.webservice.retrofit;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.DELETE;
import retrofit.http.Path;

public interface TommanSvcApi {
	public static final String TOMMAN_PATH = "/tomman";
	
	public static final String TOMMAN_FLUSH = TOMMAN_PATH + "/adm/flush";
	
	public static final String TOMMAN_INIT_TEST  = TOMMAN_PATH + "/adm/inittest";
	
	public static final String TOMMAN_ALLPATIENTS = "/adm/allpatients";
	
	public static final String TOMMAN_ALLDOCTORS = "/adm/alldoctors";
	
	public static final String TOMMAN_ASSIGN_DOCTOR_PATIENT = "/adm/assign";
	
	public static final String TOMMAN_CHECKIN = TOMMAN_PATH + "/pat/checkin";
	
	public static final String TOMMAN_HISTORY = TOMMAN_PATH + "/pat/history";
	
	public static final String TOMMAN_MEDHISTORY = TOMMAN_PATH + "/pat/medhistory";
	
	public static final String TOMMAN_MEDICATION = TOMMAN_PATH + "/doc/medication";
	
	public static final String TOMMAN_PATIENTLIST = TOMMAN_PATH + "/doc/patients";
	
	public static final String TOMMAN_PRESCRIBE = TOMMAN_PATH + "/doc/prescribe";
	
	public static final String TOMMAN_UNPRESCRIBE = TOMMAN_PATH + "/doc/unprescribe";
	
	public static final String TOMMAN_PATIENT_SELF = TOMMAN_PATH + "/pat/";

	public static final String TOMMAN_PATIENT = TOMMAN_PATH + "/doc/patient";
	
	public static final String TOMMAN_HISTORY_OF_PATIENT = TOMMAN_PATH + "/doc/history";
	
	public static final String TOMMAN_MEDHISTORY_OF_PATIENT = TOMMAN_PATH + "/doc/medhistory";
	
	public static final String TOMMAN_SEARCH_PATIENT_BY_NAME = TOMMAN_PATH + "/doc/search/";
	
	public static final String TOKEN_PATH = "/oauth/token";
	
	public static final String LOGIN = TOMMAN_PATH + "/login";
	
	public static final String TOMMAN_DOCTOR = TOMMAN_PATH + "/doc/";
	
	public static final String TOMMAN_ALERTS = TOMMAN_PATH +"/doc/alerts";
	
	@GET("/tomman/login")
	public TommanApiRole login();
	
	/* ADMIN Access */
	@DELETE(TOMMAN_FLUSH)
	public boolean flush();
	
	@POST(TOMMAN_INIT_TEST)
	public boolean initTest();
	
	@GET(TOMMAN_ALLPATIENTS)
	public Collection<PatientDTO> getAllPatients();
	
	@GET(TOMMAN_ALLDOCTORS)
	public Collection<DoctorDTO> getAllDoctors();
	
	@POST(TOMMAN_ASSIGN_DOCTOR_PATIENT + "/{did}" + "/{pid}" )
	public boolean assignDoctorToPatient(@Path("did") long doctorId, @Path("pid") long patientId);
	
	/* Doctor access*/
	@GET(TOMMAN_MEDICATION)
	public Collection<MedicationDTO> getMedications();
	
	@GET(TOMMAN_DOCTOR)
	public DoctorDTO getDoctor();
	
	@GET(TOMMAN_ALERTS + "/{ts}")
	public Collection<PatientDTO> getAlerts(@Path("ts") long timestamp);
	
	@GET(TOMMAN_MEDICATION + "/{id}")
	public MedicationDTO getMedication(@Path("id") long MedicationId);
	
	@POST(TOMMAN_MEDICATION)
	public MedicationDTO addMedication(@Body MedicationDTO med);

	@GET(TOMMAN_PATIENTLIST)
	public Collection<PatientDTO> getPatientList();
	
	@GET(TOMMAN_SEARCH_PATIENT_BY_NAME + "/{pname}")
	public Collection<PatientDTO> searchPatientByName(@Path("pname") String searchString);
	
	@POST(TOMMAN_PRESCRIBE + "/{pid}" + "/{mid}")
	public boolean prescribe(@Path("pid") long  patientId, @Path("mid") long medicationId);

	@POST(TOMMAN_UNPRESCRIBE + "/{pid}" + "/{mid}")
	public boolean unprescribe(@Path("pid") long  patientId, @Path("mid") long medicationId);
	
	@GET(TOMMAN_PATIENT + "/{id}")
	public PatientDTO getPatientInfo(@Path("id") long PatientId);
	
	@GET(TOMMAN_HISTORY_OF_PATIENT + "/{pid}")
	public Collection<CheckInDTO> getHistoryOfPatient(@Path("pid") long patientId);

	@GET(TOMMAN_MEDHISTORY_OF_PATIENT + "/{pid}")
	public Collection<TookMedicationDTO> getMedicationHistoryOfPatient(@Path("pid") long patientId);	
	
	/* 
	 * Patient Access
	 *  */
	@GET(TOMMAN_PATIENT_SELF)
	public PatientDTO getPatientInfo();
	
	@POST(TOMMAN_CHECKIN)
	public CheckInDTO checkIn(@Body CheckInDTO checkin);
	
	@GET(TOMMAN_HISTORY)
	public Collection<CheckInDTO> getHistory();
	
	@GET(TOMMAN_MEDHISTORY)
	public Collection<TookMedicationDTO> getMedicationHistory();

}