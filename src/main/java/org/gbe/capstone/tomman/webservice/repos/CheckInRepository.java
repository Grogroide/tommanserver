package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface CheckInRepository extends CrudRepository<CheckIn, Long>{
	public Collection<CheckIn> findByTimestampGreaterThan(long timestamp);
	public Collection<CheckIn> findByPatient(Patient patient);
	public Collection<CheckIn> findByPatientAndTimestampGreaterThan(Patient patient, long timestamp);
}
