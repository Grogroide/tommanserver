package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.gbe.capstone.tomman.webservice.retrofit.CheckInDTO;
import org.gbe.capstone.tomman.webservice.retrofit.TookMedicationDTO;
import org.hibernate.annotations.ManyToAny;


@Entity
public class CheckIn {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private long timestamp;
	private PainLevel painLevel;
	private EatingDifficulty eatingDifficulty;
	
	@ManyToOne
	private Patient patient;
	
	public CheckIn(){}
	
	public CheckIn(long ts, PainLevel p, EatingDifficulty e)
	{
		this.timestamp = ts;
		this.painLevel = p;
		this.eatingDifficulty = e;
	}
	
	public CheckIn(CheckInDTO dto){
		this.eatingDifficulty = dto.getEatingDifficulty();
		this.painLevel = dto.getPainLevel();
		this.timestamp = dto.getTimestamp();
	}
	
	public CheckInDTO toDto(){
		CheckInDTO dto = new CheckInDTO();
		dto.setId(id);
		dto.setTimestamp(timestamp);
		dto.setPainLevel(painLevel);
		dto.setEatingDifficulty(eatingDifficulty);
		dto.setTookMedications(new HashSet<TookMedicationDTO>());
		return dto;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public PainLevel getPainLevel() {
		return painLevel;
	}
	public void setPainLevel(PainLevel painLevel) {
		this.painLevel = painLevel;
	}
	public EatingDifficulty getEatingDifficulty() {
		return eatingDifficulty;
	}
	public void setEatingDifficulty(EatingDifficulty eatingDifficulty) {
		this.eatingDifficulty = eatingDifficulty;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
	
	//Collection<TookMedication> medicationsTaken;
}
