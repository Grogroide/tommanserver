package org.gbe.capstone.tomman.webservice.retrofit;

import org.gbe.capstone.tomman.webservice.repos.Doctor;

public class DoctorDTO implements Person{
	
	private long id;
	private String userName;
	private String firstName;
	private String lastName;
	

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
