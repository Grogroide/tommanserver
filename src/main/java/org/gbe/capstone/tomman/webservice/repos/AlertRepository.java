package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface AlertRepository extends CrudRepository<Alert, Long>{
	
	public Collection<Alert> findByTimestampGreaterThan(Long timestamp);

}
