package org.gbe.capstone.tomman.webservice.repos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.gbe.capstone.tomman.webservice.retrofit.MedicationDTO;

import com.google.common.base.Objects;

@Entity
public class Medication {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	private String description;
	
	
	
	public Medication()
	{
	}
	
	public Medication(String name, String description)
	{
		super();
		this.name = name;
		this.description = description;
	}

	public MedicationDTO toDto(){
		MedicationDTO dto = new MedicationDTO();
		dto.setId(id);
		dto.setDescription(description);
		dto.setName(name);
		return dto;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(name, description);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Medication) {
			Medication other = (Medication) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(name, other.name)
					&& Objects.equal(description, other.description);
		} else {
			return false;
		}
	}
}
