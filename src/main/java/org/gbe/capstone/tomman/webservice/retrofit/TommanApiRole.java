package org.gbe.capstone.tomman.webservice.retrofit;

public enum TommanApiRole {
	Admin,
	Doctor,
	Patient
}
