package org.gbe.capstone.tomman.webservice;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gbe.capstone.tomman.webservice.repos.Alert;
import org.gbe.capstone.tomman.webservice.repos.AlertRepository;
import org.gbe.capstone.tomman.webservice.repos.CheckIn;
import org.gbe.capstone.tomman.webservice.repos.CheckInRepository;
import org.gbe.capstone.tomman.webservice.repos.Doctor;
import org.gbe.capstone.tomman.webservice.repos.DoctorRepository;
import org.gbe.capstone.tomman.webservice.repos.EatingDifficulty;
import org.gbe.capstone.tomman.webservice.repos.Medication;
import org.gbe.capstone.tomman.webservice.repos.MedicationRepository;
import org.gbe.capstone.tomman.webservice.repos.PainLevel;
import org.gbe.capstone.tomman.webservice.repos.Patient;
import org.gbe.capstone.tomman.webservice.repos.PatientRepository;
import org.gbe.capstone.tomman.webservice.repos.TookMedication;
import org.gbe.capstone.tomman.webservice.repos.TookMedicationRepository;
import org.gbe.capstone.tomman.webservice.retrofit.CheckInDTO;
import org.gbe.capstone.tomman.webservice.retrofit.DoctorDTO;
import org.gbe.capstone.tomman.webservice.retrofit.PatientDTO;
import org.gbe.capstone.tomman.webservice.retrofit.Person;
import org.gbe.capstone.tomman.webservice.retrofit.TommanApiRole;
import org.gbe.capstone.tomman.webservice.retrofit.TommanSvcApi;
import org.gbe.capstone.tomman.webservice.retrofit.TookMedicationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;


@Controller
public class TommanController {
	
	@Autowired
	private MedicationRepository medications;
	
	@Autowired
	private PatientRepository patients;
	
	@Autowired
	private DoctorRepository doctors;
	
	@Autowired
	private CheckInRepository checkins;
	
	@Autowired
	private TookMedicationRepository tookMedications;
	
	@Autowired
	private AlertRepository alerts;
	
//	@GET("/tomman/login")
//	public Role login();
	@RequestMapping(value = TommanSvcApi.LOGIN)
	public @ResponseBody TommanApiRole login(Principal p){
		String userName = p.getName();
		if (doctors.findByUserName(userName).size() > 0){
			return TommanApiRole.Doctor;
		}
		if (patients.findByUserName(userName).size() > 0) {
			return TommanApiRole.Patient;
		}
		return null;
	}
	
	
	/* Admin functionalities */
	@RequestMapping(value = TommanSvcApi.TOMMAN_ALLPATIENTS)
	public @ResponseBody Collection<PatientDTO> getAllPatients(){
		Collection<PatientDTO> pList = new HashSet<PatientDTO>();
		for(Patient p : patients.findAll()){
			pList.add(p.toDto());
		}
		return pList;
	}
	@RequestMapping(value = TommanSvcApi.TOMMAN_ALLDOCTORS)
	public @ResponseBody Collection<DoctorDTO> getAllDoctors(){
		Collection<DoctorDTO> dList = new HashSet<DoctorDTO>();
		for(Doctor d : doctors.findAll()){
			dList.add(d.toDto());
		}
		return dList;
	}
	@RequestMapping(value = TommanSvcApi.TOMMAN_ASSIGN_DOCTOR_PATIENT + "/{did}" + "/{pid}", method=RequestMethod.POST)
	public @ResponseBody boolean assignDoctorToPatient(
			@PathVariable("did") long doctorId, 
			@PathVariable("pid") long patientId,
			HttpServletResponse resp){
		Doctor d = doctors.findOne(doctorId);
		Patient p = patients.findOne(patientId);
		Collection<Patient> pList = d.getPatients();
		Collection<Doctor> dList = p.getDoctors();
		if (pList.contains(p) || dList.contains(d)){
			//resp.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return true;
		}
		else{
			pList.add(p);
			dList.add(d);
			p.setDoctors(dList);
			d.setPatients(pList);
			if (doctors.save(d).getPatients().contains(p)  && patients.save(p).getDoctors().contains(d)) {
				return true;
			}
		}
		return false;
	}
	/* MEDICATION API*/
	@RequestMapping(value=TommanSvcApi.TOMMAN_MEDICATION, method=RequestMethod.POST)
	public @ResponseBody Medication addMedication(@RequestBody Medication m, HttpServletResponse resp)
	{
		String name = m.getName();
		String description = m.getDescription();
		Collection<Medication> mList = medications.findByNameAndDescription(name, description);
		switch(mList.size())
		{
		case 0:
			return medications.save(m);
		case 1:
			return mList.iterator().next();
		default:
				resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return null;
		}
	}
	@RequestMapping(value=TommanSvcApi.TOMMAN_MEDICATION + "/{id}", method=RequestMethod.GET)
	public @ResponseBody Medication getMedication(@PathVariable("id") long id)
	{
		return medications.findOne(id);
	}
	@RequestMapping(value=TommanSvcApi.TOMMAN_MEDICATION, method=RequestMethod.GET)
	public @ResponseBody Collection<Medication> getMedications()
	{
		return (Collection<Medication>) medications.findAll();
	}	
	@RequestMapping(value=TommanSvcApi.TOMMAN_PATIENT_SELF, method=RequestMethod.GET)
	public @ResponseBody Person getPatientInfo(Principal p, HttpServletResponse resp)
	{
		Collection<Patient> list = patients.findByUserName(p.getName());
		if ( list == null || list.size() == 0)
		{
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		for (Patient patient : list) {
			if (patient.getUserName() == p.getName())
			{
				return patient.toDto();
			}
		}
		resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}
	@RequestMapping(value = TommanSvcApi.TOMMAN_PATIENT + "/{id}", method=RequestMethod.GET)
	public @ResponseBody Person getPatientInfo(
			@PathVariable("id") long patientId,
			Principal p, 
			HttpServletResponse resp)
	{
		Doctor self = getDoctorByExactName(p.getName());
		Patient patient = patients.findOne(patientId);
		if(self == null) {
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		if (patient == null){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		if (self.getPatients().contains(patient)){
			return patient.toDto();		
		}
		resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}
	
//	@GET(TOMMAN_DOCTOR)
//	public DoctorDTO getDoctor();
	@RequestMapping(value = TommanSvcApi.TOMMAN_DOCTOR)
	public @ResponseBody DoctorDTO getDoctor(Principal p){
		return doctors.findByUserName(p.getName()).iterator().next().toDto();  //TODO : ugly
	}
	
	@RequestMapping(value = TommanSvcApi.TOMMAN_PATIENTLIST, method=RequestMethod.GET)
	public @ResponseBody Collection<PatientDTO> getPatientList(
			Principal p,
			HttpServletResponse resp)
	{
		Doctor self = getDoctorByExactName(p.getName());
		if(self == null) {
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		Collection<Patient> pList = self.getPatients(); //patients.findAll(); //self.getPatients();
		Collection<PatientDTO> pListDTO = new HashSet<PatientDTO>();
		if(pList.isEmpty())
		{
			for(Patient patient : pList){
				pListDTO.add(patient.toDto());
			}
			return pListDTO;
		}else{
			for(Patient patient : pList){
				pListDTO.add(patient.toDto());
			}
			return pListDTO;
		}
	}
	
//	@GET(TOMMAN_SEARCH_PATIENT_BY_NAME + "/{pname}")
//	public Collection<PatientDTO> searchPatientByName(@Path("pname") String searchString);
	
	@RequestMapping(value = TommanSvcApi.TOMMAN_SEARCH_PATIENT_BY_NAME + "{pname}")
	public @ResponseBody Collection<PatientDTO> searchPatientByName(
			Principal p,
			HttpServletResponse resp,
			@PathVariable("pname") String searchString)
			{
				Doctor doc = doctors.findOneByUserName(p.getName());
				Collection<Patient> list  = patients.findByFirstNameOrLastName(searchString, searchString);
				Collection<PatientDTO> dtoList = new ArrayList<PatientDTO>();
				for (Patient patient : list) {
					if (patient.getDoctors().contains(doc)) 
						dtoList.add(patient.toDto());
				}
				return dtoList;
			}
	
	@RequestMapping(value = TommanSvcApi.TOMMAN_PRESCRIBE + "/{pid}" + "/{mid}", method=RequestMethod.POST)
	public @ResponseBody boolean prescribe(
			@PathVariable("pid") long patientId, 
			@PathVariable("mid") long medicationId, 
			HttpServletResponse resp,
			Principal p)
	{
		Doctor self = getDoctorByExactName(p.getName());
		Patient pat = patients.findOne(patientId);
		Medication med = medications.findOne(medicationId);
		if(self == null) {
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		if (pat == null || med == null)
		{
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		if(!self.getPatients().contains(pat)){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		Collection<Medication> prescriptions = pat.getPrescriptions();
		if (prescriptions.contains(med))
		{
			return true;
		}
		prescriptions.add(med);
		pat.setPrescriptions(prescriptions);
		if(patients.save(pat).getPrescriptions().contains(med)){
			return true;
		}
		return false;
	}
//	@POST(TOMMAN_UNPRESCRIBE + "/{pid}" + "/{mid}")
//	public boolean unprescribe(@Path("pid") long  patientId, @Path("mid") long medicationId);
	@RequestMapping(value = TommanSvcApi.TOMMAN_UNPRESCRIBE + "/{pid}" + "/{mid}", method=RequestMethod.POST)
	public @ResponseBody boolean unprescribe(
			@PathVariable("pid") long patientId, 
			@PathVariable("mid") long medicationId, 
			HttpServletResponse resp,
			Principal p)
	{
		Doctor self = getDoctorByExactName(p.getName());
		Patient pat = patients.findOne(patientId);
		Medication med = medications.findOne(medicationId);
		if(self == null) {
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		if (pat == null || med == null)
		{
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		if(!self.getPatients().contains(pat)){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		Collection<Medication> prescriptions = pat.getPrescriptions();
		if (!prescriptions.contains(med))
		{
			return true;
		}
		prescriptions.remove(med);
		pat.setPrescriptions(prescriptions);
		if(!patients.save(pat).getPrescriptions().contains(med)){
			return true;
		}
		return false;
	}
	
//	@GET(TOMMAN_ALERTS + "/{ts}")
//	public Collection<PatientDTO> getAlerts(@Path("ts") long timestamp);
	@RequestMapping(value=TommanSvcApi.TOMMAN_ALERTS + "/{ts}")
	public @ResponseBody Collection<PatientDTO> getAlerts(
			@PathVariable("ts") long timestamp,
			Principal p) {
		Doctor doc = doctors.findOneByUserName(p.getName());
		Collection<Alert> recentAlerts = alerts.findByTimestampGreaterThan(timestamp);
		Collection<PatientDTO> result = new HashSet<PatientDTO>();
		for (Alert alert : recentAlerts) {
			if(alert.getPatient().getDoctors().contains(doc)) {
				result.add(alert.getPatient().toDto());
			}
		}
		return result;
	}
	
	
//	@POST(TOMMAN_CHECKIN)
//	public boolean checkIn(@Body CheckIn checkin);
	@RequestMapping(value=TommanSvcApi.TOMMAN_CHECKIN, method=RequestMethod.POST)
	public @ResponseBody CheckInDTO checkIn(
			@RequestBody CheckInDTO dto,
			HttpServletResponse resp,
			Principal pr){
		CheckIn c = new CheckIn(dto);
		Collection<TookMedicationDTO> tmList = dto.getTookMedications();
		Patient p = patients.findByUserName(pr.getName()).iterator().next();
		
		for (TookMedicationDTO tmDTO : tmList) {
			TookMedication tm = new TookMedication(tmDTO);
			tm.setPatient(patients.findOne(tmDTO.getPatientId()));
			tm.setMedication(medications.findOne(tmDTO.getMedicationId()));
			p.getTookMedications().add(tm);
			tookMedications.save(tm);
		}
		c.setPatient(p);
		p.getCheckins().add(c);
		
		CheckIn rv = checkins.save(c);
		
		if (checkForAlert(p, rv)){
			Alert alert = new Alert();
			alert.setTimestamp(rv.getTimestamp());
			alert.setPatient(p);
			alerts.save(alert);
		}

		patients.save(p);
		return rv.toDto();  // the TookMedications is amputated
	}
	
	private boolean checkForAlert(Patient p, CheckIn ci) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ci.getTimestamp());
		Calendar sixteenHoursAgo = Calendar.getInstance();
		sixteenHoursAgo.setTimeInMillis(ci.getTimestamp());
		sixteenHoursAgo.add(Calendar.HOUR_OF_DAY, -16);
		Calendar twelveHoursAgo = Calendar.getInstance();
		twelveHoursAgo.setTimeInMillis(ci.getTimestamp());
		twelveHoursAgo.add(Calendar.HOUR_OF_DAY, -16);
		Collection<CheckIn> recentHistory = checkins.findByPatientAndTimestampGreaterThan(p, twelveHoursAgo.getTimeInMillis());
		
		if (allReportSeverePain(recentHistory) || allReportCantEat(recentHistory))
			return true;
		
		recentHistory = checkins.findByPatientAndTimestampGreaterThan(p, sixteenHoursAgo.getTimeInMillis());
		if (allReportModerateOrSeverePain(recentHistory))
			return true;
		
 		return false;
	}
	
	
	
	private boolean allReportModerateOrSeverePain(Collection<CheckIn> recentHistory) {
		if(recentHistory.size() <= 2)
			return false;
		for (CheckIn checkIn : recentHistory) {
			if (checkIn.getPainLevel() == PainLevel.WELL_CONTROLLED)
				return false;
		}
		return true;
	}


	private boolean allReportCantEat(Collection<CheckIn> recentHistory) {
		//boolean rv = true;
		if (recentHistory.size() <= 2)			// don't want to trigger an alert on the first two reports.
			return false;
		for (CheckIn checkIn : recentHistory) {
			if (checkIn.getEatingDifficulty() != EatingDifficulty.I_CANT_EAT)
				return false;
		}
		return true;
	}


	private boolean allReportSeverePain(Collection<CheckIn> recentHistory) {
//		boolean rv = true;
		if (recentHistory.size() <= 2)			// don't want to trigger an alert on the first two reports.
			return false;
		for (CheckIn checkIn : recentHistory) {
			if (checkIn.getPainLevel() != PainLevel.SEVERE)
				return false;
		}
		return true;
	}


	//	@GET(TOMMAN_HISTORY)
//	public Collection<CheckIn> getHistory();
	@RequestMapping(value=TommanSvcApi.TOMMAN_HISTORY, method=RequestMethod.GET)
	public @ResponseBody Collection<CheckInDTO> getHistory(
			Principal pr) {
		Patient p = patients.findByUserName(pr.getName()).iterator().next();
		Collection<CheckIn> ciList = p.getCheckins();
		Collection<CheckInDTO> dtoList = new HashSet<CheckInDTO> ();
		for (CheckIn checkIn : ciList) {
			dtoList.add(checkIn.toDto());
		}
		return dtoList;
	}
	
//	@GET(TOMMAN_MEDHISTORY)
//	public Collection<TookMedicationDTO> getMedicationHistory();
	@RequestMapping(value = TommanSvcApi.TOMMAN_MEDHISTORY)
	public @ResponseBody Collection<TookMedicationDTO> getMedicationHistory(
			Principal pr){
		Patient p = patients.findByUserName(pr.getName()).iterator().next();
		Collection<TookMedication> tmList = p.getTookMedications();
		Collection<TookMedicationDTO> dtoList = new HashSet<TookMedicationDTO> ();
		for (TookMedication tm : tmList) {
			TookMedicationDTO dto = tm.toDto();
			//re-inject the medicationDTO at this level, so the DTO have all the information necessary for the client
			dto.setMedication(medications.findOne(dto.getMedicationId()).toDto());  
			dtoList.add(dto);
		}
		return dtoList;
	}
	
	
//	@GET(TOMMAN_HISTORY_OF_PATIENT + "/{pid}")
//	public Collection<CheckInDTO> getHistoryOfPatient(@Path("pid") long patientId);
	@RequestMapping(value=TommanSvcApi.TOMMAN_HISTORY_OF_PATIENT + "/{pid}")
	public @ResponseBody Collection<CheckInDTO> getHistoryOfPatient(
			@PathVariable("pid") long patientId,
			HttpServletResponse resp,
			Principal pr){
		Patient p = patients.findOne(patientId);
		Doctor d = getDoctorByExactName(pr.getName());
		if (!d.getPatients().contains(p)){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}else{
			Collection<CheckInDTO> dtoList = new HashSet<CheckInDTO>();
			Collection<CheckIn> ciList = p.getCheckins();
			for (CheckIn checkIn : ciList) {
				dtoList.add(checkIn.toDto());
			}
			return dtoList;
		}
	}
	
//	@GET(TOMMAN_MEDHISTORY_OF_PATIENT + "/{pid}")
//	public Collection<TookMedicationDTO> getMedicationHistoryOfPatient();
	@RequestMapping(value=TommanSvcApi.TOMMAN_MEDHISTORY_OF_PATIENT + "/{pid}")
	public @ResponseBody Collection<TookMedicationDTO> getMedicationHistoryOfPatient(
			@PathVariable("pid") long patientId,
			HttpServletResponse resp,
			Principal pr
			){
		Patient p = patients.findOne(patientId);
		Doctor d = getDoctorByExactName(pr.getName());
		if (!d.getPatients().contains(p)){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}else{
			Collection<TookMedicationDTO> dtoList = new HashSet<TookMedicationDTO>();
			Collection<TookMedication> tmList = p.getTookMedications();
			for (TookMedication tm : tmList) {
				TookMedicationDTO dto = tm.toDto();
				//re-inject the medicationDTO at this level, so the DTO have all the information necessary for the client
				dto.setMedication(medications.findOne(dto.getMedicationId()).toDto());  
				dtoList.add(dto);
			}
			return dtoList;
		}
	}
	
//	@DELETE(TOMMAN_FLUSH)
//	public boolean flush();
	@RequestMapping(value=TommanSvcApi.TOMMAN_FLUSH)
	public @ResponseBody boolean flush(){
		deleteAllRepos();
		return true;
	}
	
//	@POST(TOMMAN_INIT_TEST)
//	public boolean initTest();
	@RequestMapping(value=TommanSvcApi.TOMMAN_INIT_TEST)
	public @ResponseBody boolean initTest(HttpServletResponse resp){
		try {
			createTestEntities();
			return true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return false;
		}
	}
	
	
	/* This function initializes the data in the various repositories, as Patient/Doctor creation is not yet supported
	 * It fits the Users created in the OAuth2SecurityConfiguration
	 * */
	@PostConstruct
	public void populateDB() throws ParseException
	{
		deleteAllRepos();
		createTestEntities();
	}
	private void createTestEntities() throws ParseException {
		Medication med = new Medication("Aspirin 500mg", "Take with meal");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Patient p1 = new Patient("patient1", "Paulie", "Gualtieri", "123-45-677", sdf.parse("1962-12-23").getTime());
		Patient p2 = new Patient("patient2", "Silvio", "Dante", "123-45-678", sdf.parse("1972-06-02").getTime());
		Patient p3 = new Patient("patient3", "Furio", "Giunta", "123-45-679", sdf.parse("1980-06-02").getTime());
		Doctor d1 = new Doctor("doctor1", "Jennifer", "Melfi");
		Doctor d2 = new Doctor("doctor2", "Bruce", "Cusamano");
		
		med = medications.save(med);
		p1 = patients.save(p1);
		p2 = patients.save(p2);
		p3 = patients.save(p3);
		d1 = doctors.save(d1);
		d2 = doctors.save(d2);
	}
	
	private void deleteAllRepos() {
		//checkins.deleteAll(); //before deleting
		Iterable<Alert> aList = alerts.findAll();
		for (Alert alert : aList) {
			alerts.delete(alert);
		}
		Iterable<TookMedication> tmList = tookMedications.findAll();
		for (TookMedication tookMedication : tmList) {
			tookMedications.delete(tookMedication);
		}

		Iterable<CheckIn> ciList = checkins.findAll();
		for (CheckIn checkIn : ciList) {
			checkins.delete(checkIn);
		}
		Iterable<Doctor> dList = doctors.findAll();
		for (Doctor doctor : dList) {
			doctors.delete(doctor);
		}
		Iterable<Patient> pList = patients.findAll();
		for (Patient patient : pList) {
			patients.delete(patient);
		}
		Iterable<Medication> mList = medications.findAll();
		for (Medication medication : mList) {
			medications.delete(medication);
		}

	}
	
	private Doctor getDoctorByExactName(String name) {
		Collection<Doctor> docList = doctors.findByUserName(name);
		Doctor self = null;
		for (Doctor doctor : docList) {
			if (doctor.getUserName() == name)
				self = doctor;
		}
		return self;
	}
	
	
	private void link( Patient p, Doctor d)
	{
		//p.addDoctor(d);
		//d.addPatient(p);
		Collection<Patient> pList = d.getPatients();
		Collection<Doctor> dList = p.getDoctors();
		
		pList.add(p);
		dList.add(d);
		
		d.setPatients(pList);
		p.setDoctors(dList);
	}
	private void link(Doctor d, Patient p)
	{
		link(p, d);
	}
}
