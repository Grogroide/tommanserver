package org.gbe.capstone.tomman.webservice.repos;

import org.springframework.data.repository.CrudRepository;

public interface TookMedicationRepository extends CrudRepository<TookMedication, Long>{

}
