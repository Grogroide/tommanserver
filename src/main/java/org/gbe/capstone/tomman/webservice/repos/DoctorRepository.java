package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface DoctorRepository extends CrudRepository<Doctor, Long>{

	Collection<Doctor> findByUserName(String userName);
	Doctor findOneByUserName(String userName);

}
