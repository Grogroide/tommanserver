package org.gbe.capstone.tomman.webservice.retrofit;

public interface Person {

	public abstract long getId();

	public abstract void setId(long id);

	public abstract String getFirstName();

	public abstract void setFirstName(String firstName);

	public abstract String getLastName();

	public abstract void setLastName(String lastName);

	public abstract String getUserName();

	public abstract void setUserName(String userName);

}