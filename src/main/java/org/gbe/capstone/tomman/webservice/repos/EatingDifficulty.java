package org.gbe.capstone.tomman.webservice.repos;

public enum EatingDifficulty {
	NO,
	SOME,
	I_CANT_EAT
}
