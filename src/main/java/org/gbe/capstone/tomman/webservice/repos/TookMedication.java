package org.gbe.capstone.tomman.webservice.repos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.gbe.capstone.tomman.webservice.retrofit.TookMedicationDTO;


@Entity
public class TookMedication {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long timeStamp;
	private int quantity;
	
	@ManyToOne
	private	Medication medication;
	
	@ManyToOne
	private	Patient patient;
	
	public TookMedication(){}
	
	public TookMedication(TookMedicationDTO dto) {
		this.id = dto.getId();
		this.timeStamp = dto.getTimestamp();
		this.quantity = dto.getQuantity();
	}
	
	public TookMedicationDTO toDto() {
		TookMedicationDTO dto = new TookMedicationDTO();
		dto.setId(id);
		dto.setTimestamp(timeStamp);
		dto.setMedicationId(medication.getId());
		dto.setPatientId(patient.getId());
		dto.setQuantity(quantity);		
		return dto;
	}
	
	public Medication getMedication() {
		return medication;
	}
	public void setMedication(Medication medication) {
		this.medication = medication;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
}
