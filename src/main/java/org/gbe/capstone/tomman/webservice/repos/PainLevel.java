package org.gbe.capstone.tomman.webservice.repos;

public enum PainLevel {
	WELL_CONTROLLED, 
	MODERATE, 
	SEVERE
}

