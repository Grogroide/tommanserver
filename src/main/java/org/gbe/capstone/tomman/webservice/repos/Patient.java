package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.gbe.capstone.tomman.webservice.retrofit.MedicationDTO;
import org.gbe.capstone.tomman.webservice.retrofit.PatientDTO;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.None.class, property="id")
public class Patient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstName;
	private String lastName;
	private String userName;
	private String medicalRecordNumber;
	private long dateOfBirth;
	
	@OneToMany(mappedBy = "patient")
	private Collection<CheckIn> checkins;
	
	@OneToMany(mappedBy = "patient")
	private Collection<Alert> alerts;
	
	@OneToMany(mappedBy = "patient")
	private Collection<TookMedication> tookMedications;
	
	@ManyToMany
	private Collection<Medication> prescriptions;
	
	//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
	//@JsonIdentityInfo(generator = ObjectIdGenerators.None.class, property="@id")
	@ManyToMany(mappedBy="patients")
	private Collection<Doctor> doctors;
	
	public Patient() {
	}
	
	public Patient(String userName, String firstName, String lastName, String medicalRecordNumber, long dateOfBirth){
		super();
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.medicalRecordNumber = medicalRecordNumber;
	}
	
	public PatientDTO toDto() {
		PatientDTO dto = new PatientDTO();
		dto.setId(id);
		dto.setUserName(userName);
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setMedicalRecordNumber(medicalRecordNumber);
		dto.setDateOfBirth(dateOfBirth);
			
		dto.setPrescriptions(new HashSet<MedicationDTO>());
		for (Medication med : prescriptions) {
			dto.getPrescriptions().add(med.toDto());
		}
			
		dto.setDoctors(new HashSet<Long>());
		for(Doctor doctor : doctors){
			dto.getDoctors().add(doctor.getId());
		}
		return dto;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Collection<Medication> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(Collection<Medication> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public Collection<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(Collection<Doctor> doctors) {
		this.doctors = doctors;
	}

	public String getMedicalRecordNumber() {
		return medicalRecordNumber;
	}

	public void setMedicalRecordNumber(String medicalRecordNumber) {
		this.medicalRecordNumber = medicalRecordNumber;
	}

	public Collection<CheckIn> getCheckins() {
		return checkins;
	}

	public void setCheckins(Collection<CheckIn> checkins) {
		this.checkins = checkins;
	}

	public Collection<TookMedication> getTookMedications() {
		return tookMedications;
	}

	public void setTookMedications(Collection<TookMedication> tookMedications) {
		this.tookMedications = tookMedications;
	}
}
