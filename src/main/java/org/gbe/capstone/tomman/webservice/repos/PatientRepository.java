package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface PatientRepository extends CrudRepository<Patient, Long>{

	public Collection<Patient> findByUserName(String userName);
	public Collection<Patient> findByFirstNameOrLastName(String firstName, String lastName);
}
