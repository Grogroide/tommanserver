package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long>{

	Collection<Medication> findByName(String name);

	Collection<Medication> findByNameAndDescription(String name,
			String description);
}
