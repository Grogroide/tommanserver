package org.gbe.capstone.tomman.webservice.repos;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.gbe.capstone.tomman.webservice.retrofit.DoctorDTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.None.class, property="id")
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String userName;
	private String firstName;
	private String lastName;
	
	@ManyToMany
	//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
	//@JsonIdentityInfo(generator = ObjectIdGenerators.None.class, property="@id")
	private Collection<Patient> patients;
	
	public Doctor(){
		
	}

	public Doctor(String username, String firstname, String lastname){
		super();
		this.userName = username;
		this.firstName = firstname;
		this.lastName = lastname;
	}
	public DoctorDTO toDto(){
		DoctorDTO dto = new DoctorDTO();
		dto.setId(id);
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setUserName(userName);
		return dto;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Collection<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Collection<Patient> patients) {
		this.patients = patients;
	}
	
//	public boolean addPatient(Patient p)
//	{
//		return patients.add(p);
//	}
//	
//	public boolean removePatient(Patient p)
//	{
//		return patients.remove(p);
//	}	
}
